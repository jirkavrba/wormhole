import Config

config :nostrum,
  gateway_intents: [
    :guilds,
    :guild_members,
    :guild_messages,
    :guild_webhooks,
    :message_content
  ]

config :nosedrum,
  prefix: "--"

if config_env() != :prod and File.exists?("config/credentials.exs") do
  import_config("credentials.exs")
end
