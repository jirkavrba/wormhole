FROM elixir:1.14.2 AS build

COPY mix.exs  .
COPY mix.lock . 

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix deps.compile

COPY config   ./config
COPY lib      ./lib

RUN MIX_ENV=prod mix release


FROM elixir:1.14.2-slim AS runtime

RUN mkdir /workspace

COPY --from=build _build/prod/rel/wormhole /workspace

CMD ["/workspace/bin/wormhole", "start"]