defmodule Wormhole.ConnectionPool do
  use GenServer
  require Logger

  alias Wormhole.Connection

  @doc "Request a new connection withing pending requests on the pool."
  @spec request_connection(integer(), (Connection.t() -> any())) :: no_return()
  def request_connection(channel_id, callback) do
    GenServer.cast(__MODULE__, {:request_connection, {channel_id, callback}})
  end

  @doc "Get a connection for the provided channel. Returns `:not_connected` if there is no open connection."
  @spec get_connection(integer()) :: Connection.t() | :not_connected
  def get_connection(channel_id) do
    GenServer.call(__MODULE__, {:get_connection, channel_id})
  end

  @doc "Terminate a connection from the provided channel."
  @spec terminate_connection(integer()) :: :ok | :not_connected
  def terminate_connection(channel_id) do
    GenServer.call(__MODULE__, {:terminate_connection, channel_id})
  end

  def start_link(_init) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @impl true
  def init(_) do
    initial_state = %{
      pending_requests: [],
      connections: Map.new()
    }

    {:ok, initial_state}
  end

  # There are no pending requests that can the current connection be paired with
  @impl true
  def handle_cast({:request_connection, request}, %{pending_requests: []} = state) do
    new_state = %{
      pending_requests: [request],
      connections: state.connections
    }

    {:noreply, new_state}
  end

  # There is at least one another pending request that the current connection can be paired with
  def handle_cast({:request_connection, {channel_id, callback} = request}, state) do
    candidates =
      state.pending_requests
      |> Enum.filter(fn {channel, _} -> channel !== channel_id end)
      |> Enum.shuffle()

    if Enum.empty?(candidates) do
      new_state = %{
        pending_requests: [request],
        connections: state.connections
      }

      {:noreply, new_state}
    else
      [{matched_channel, matched_callback} | remaining] = candidates
      channels = [channel_id, matched_channel]

      connection = Connection.new(channels)

      connections =
        Enum.reduce(channels, state.connections, fn channel, connections ->
          Map.put(connections, channel, connection)
        end)

      Logger.info(
        "Created a new connection with ID [#{connection.id}] between the channels: #{Enum.join(connection.connected_channels, ",")}"
      )

      # Run connection callbacks
      Enum.each([callback, matched_callback], fn callback -> callback.(connection) end)

      new_state = %{
        pending_requests: remaining,
        connections: connections
      }

      {:noreply, new_state}
    end
  end

  @impl true
  def handle_call({:get_connection, channel_id}, _from, state) do
    case Map.get(state.connections, channel_id) do
      nil -> {:reply, :not_connected, state}
      connection -> {:reply, connection, state}
    end
  end

  @impl true
  def handle_call({:terminate_connection, channel_id}, _from, state) do
    case Map.get(state.connections, channel_id) do
      nil ->
        {:reply, :not_connected, state}

      %Connection{connected_channels: channels} = connection ->
        Logger.info("Terminating the connection with ID [#{connection.id}]")
        Connection.terminate(connection)

        updated_connections = Map.drop(state.connections, channels)
        updated_states = %{state | connections: updated_connections}

        {:reply, :ok, updated_states}
    end
  end
end
