defmodule Wormhole.Discord.Consumer do
  use Nostrum.Consumer

  alias Wormhole.Connection
  alias Wormhole.ConnectionPool
  alias Wormhole.Discord.Embeds
  alias Nostrum.Struct.Message
  alias Nostrum.Struct.User
  alias Nostrum.Consumer
  alias Nostrum.Api

  alias Nosedrum.Invoker.Split, as: CommandInvoker
  alias Nosedrum.Storage.ETS, as: CommandStorage

  @text_commands %{
    # Aliases to make things easy for yggdrasil users
    ["connect", "userphone"] => Wormhole.Discord.Commands.Connect.TextCommand,
    ["disconnect", "hangup"] => Wormhole.Discord.Commands.Disconnect.TextCommand,
  }

  @slash_commands %{
    "connect" => Wormhole.Discord.Commands.Connect.SlashCommand,
    "disconnect" => Wormhole.Discord.Commands.Disconnect.SlashCommand
  }

  @spec start_link() :: Supervisor.on_start()
  def start_link do
    Consumer.start_link(__MODULE__)
  end

  @impl true
  @spec handle_event(Nostrum.Consumer.event()) :: any()
  def handle_event({:READY, _event, _ws_state}) do
    Api.update_status(:online, "tag me to get started")

    @text_commands
    |> Enum.flat_map(fn {names, module} -> Enum.map(names, fn name -> {name, module} end) end)
    |> Enum.map(fn {name, module} -> CommandStorage.add_command([name], module) end)
    |> Enum.map(&IO.inspect/1)

    @slash_commands
    |> Enum.map(fn {name, command} -> Nosedrum.Interactor.Dispatcher.add_command(name, command, :global) end)
    |> Enum.map(&IO.inspect/1)
  end

  def handle_event({:MESSAGE_CREATE, %Message{} = message, _ws_state}) do
    if message.author.bot == nil do
      bot = Api.get_current_user!()
      mentioned = Enum.any?(message.mentions, fn %User{id: id} -> id == bot.id end)

      case CommandInvoker.handle_message(message, CommandStorage) do
        :ignored -> if (mentioned), do: handle_help_command(message), else: handle_channel_message(message)
        _message -> :noop
      end
    end
  end

  def handle_event({:INTERACTION_CREATE, interaction, _ws_state}) do
    Nosedrum.Interactor.Dispatcher.handle_interaction(interaction)
  end

  def handle_event(_event) do
    :noop
  end

  @spec handle_channel_message(Message.t()) :: no_return()
  defp handle_channel_message(message) do
    case ConnectionPool.get_connection(message.channel_id) do
      # Ignore the message
      :not_connected -> nil
      connection -> Connection.transmit_message(connection, message)
    end
  end

  @spec handle_help_command(Message.t()) :: no_return()
  defp handle_help_command(message) do
    Api.create_message!(
      message,
      embeds: [Embeds.help()],
      message_reference: %{message_id: message.id}
    )
  end
end
