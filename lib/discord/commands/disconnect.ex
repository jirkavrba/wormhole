defmodule Wormhole.Discord.Commands.Disconnect do
  alias Wormhole.Discord.Commands.Connect.SlashCommand

  defmodule TextCommand do
    @behaviour Nosedrum.Command

    alias Nostrum.Api
    alias Wormhole.ConnectionPool
    alias Wormhole.Discord.Embeds

    @impl true
    def command(message, _args) do
      if ConnectionPool.terminate_connection(message.channel_id) == :not_connected do
        Api.create_message!(
          message,
          embeds: [Embeds.not_connected()],
          message_reference: %{message_id: message.id}
        )
      end
    end

    @impl true
    def description,
      do: "Terminate the current Wormhole connection that's linked to this channel."

    @impl true
    def predicates, do: []

    @impl true
    def usage, do: []
  end

  defmodule SlashCommand do
    @behaviour Nosedrum.ApplicationCommand

    alias Nostrum.Struct.Interaction
    alias Wormhole.ConnectionPool
    alias Wormhole.Discord.Embeds

    @impl true
    def command(%Interaction{} = interaction) do
      embed =
        if ConnectionPool.terminate_connection(interaction.channel_id) == :not_connected,
          do: Embeds.not_connected(),
          else: Embeds.connection_terminated()

      [
        type: :channel_message_with_source,
        embeds: [embed],
        ephemeral?: false
      ]
    end

    @impl true
    def description,
      do: "Terminate the current Wormhole connection that's linked to this channel."

    @impl true
    def type, do: :slash

    @impl true
    def options, do: []
  end
end
