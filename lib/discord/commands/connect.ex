defmodule Wormhole.Discord.Commands.Connect do

  defmodule TextCommand do
    @behaviour Nosedrum.Command

    alias Nostrum.Api
    alias Wormhole.ConnectionPool
    alias Wormhole.Discord.Embeds

    @impl true
    def command(message, _args) do
      status_message =
        Api.create_message!(
          message,
          embeds: [Embeds.connecting()],
          message_reference: %{message_id: message.id}
        )

      ConnectionPool.request_connection(message.channel_id, fn connection ->
        Api.edit_message(status_message, embeds: [Embeds.connected(connection.id)])
      end)
    end

    @impl true
    def description, do: "Connect to the Wormhole pool and link this channel to another server"

    @impl true
    def usage, do: []

    @impl true
    def predicates, do: []
  end

  defmodule SlashCommand do
    @behaviour Nosedrum.ApplicationCommand

    alias Nostrum.Api
    alias Nostrum.Struct.Interaction
    alias Wormhole.ConnectionPool
    alias Wormhole.Discord.Embeds

    @impl true
    def command(%Interaction{} = interaction) do
      ConnectionPool.request_connection(interaction.channel_id, fn connection ->
        Api.edit_interaction_response(interaction, %{ embeds: [Embeds.connected(connection.id)]})
      end)

      [
        type: :channel_message_with_source,
        embeds: [Embeds.connecting()],
        ephemeral?: false
      ]
    end

    @impl true
    def description, do: "Connect to the Wormhole pool and link this channel to another server"

    @impl true
    def type, do: :slash

    @impl true
    def options, do: []
  end
end
