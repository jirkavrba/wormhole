defmodule Wormhole.Discord.Embeds do
  alias Nostrum.Struct.Embed

  @project_image "https://gitlab.com/uploads/-/system/project/avatar/42068222/wormhole.png?width=256"
  @project_thumbnail "https://gitlab.com/uploads/-/system/project/avatar/42068222/wormhole.png?width=64"

  @spec help :: Embed.t()
  def help do
    %Embed{}
    |> Embed.put_title("Welcome to Wormhole!")
    |> Embed.put_description("""
    Wormhole is a bot that allows you to talk to strangers from other Discord servers.

    Available commands:
    - To start a new call, use `/connect`, `--connect` or `--userphone`
    - To end the current call, use `/disconnect`, `--disconnect` or `--hangup`
    """)
    |> Embed.put_color(0xFFF5ED)
    |> Embed.put_image(@project_image)
    |> Embed.put_footer(
      "https://gitlab.com/jirkavrba/wormhole",
      "https://about.gitlab.com/images/press/logo/png/gitlab-logo-500.png"
    )
  end

  @spec connecting :: Embed.t()
  def connecting do
    %Embed{}
    |> Embed.put_title("Connecting you to a wormhole...")
    |> Embed.put_description("""
      Waiting for another open Wormhole connection.

      This can take a while, because not many people are using Wormhole yet.

      The waiting time will significantly improve in the future with more people using it.

      This bot is focused around not hanging existing connections and therefore the wait is required.
    """)
    |> Embed.put_color(0xFFF5ED)
    |> Embed.put_thumbnail(@project_thumbnail)
  end

  @spec connected(binary()) :: Embed.t()
  def connected(connection_id) do
    %Embed{}
    |> Embed.put_title("You're now connected!")
    |> Embed.put_description("To hang up the connection, please use `/disconnect`, `--disconnect` or `--hangup`")
    |> Embed.put_color(0x57F287)
    |> Embed.put_thumbnail(@project_thumbnail)
    |> Embed.put_footer("Connection id: #{connection_id}")
  end

  @spec not_connected() :: Embed.t()
  def not_connected do
    %Embed{}
    |> Embed.put_title("You're not connected to any other wormhole.")
    |> Embed.put_description("To start a new connection, please use `/connect`, `--connect` or `--userphone`")
    |> Embed.put_color(0xED4245)
    |> Embed.put_thumbnail(@project_thumbnail)
  end

  @spec disconnected() :: Embed.t()
  def disconnected do
    %Embed{}
    |> Embed.put_title("The wormhole connection was terminated by one of the parties.")
    |> Embed.put_description(
      "We hope you had fun talking with strangers.\nTo connect to a new wormhole connection, please use `/connect`, `--connect` or `--userphone`."
    )
    |> Embed.put_color(0xED4245)
    |> Embed.put_thumbnail(@project_thumbnail)
  end

  @spec connection_terminated() :: Embed.t()
  def connection_terminated do
    %Embed{}
    |> Embed.put_title("The wormhole connection was terminated")
    |> Embed.put_color(0xFFF5ED)
    |> Embed.put_thumbnail(@project_thumbnail)
  end
end
