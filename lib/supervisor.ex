defmodule Wormhole.Supervisor do
  use Supervisor

  def start(_mode, args) do
    Supervisor.start_link(__MODULE__, args, name: __MODULE__)
  end

  @impl Supervisor
  def init(_init_args) do
    children = [
      Nosedrum.Storage.ETS,
      Nosedrum.Interactor.Dispatcher,
      Wormhole.Discord.Consumer,
      Wormhole.ConnectionPool
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
