defmodule Wormhole.Connection do
  alias Nostrum.Struct.User
  alias Nostrum.Struct.Embed
  alias Nostrum.Api
  alias Nostrum.Struct.Message
  alias Nostrum.Struct.Message.Attachment
  alias Wormhole.Discord.Embeds

  @type t :: %__MODULE__{
          id: String.t(),
          connected_channels: list(integer())
        }

  @enforce_keys [:id, :connected_channels]
  defstruct [:id, :connected_channels]

  @spec new(list(integer())) :: t()
  def new(connected_channels) when is_list(connected_channels) do
    %__MODULE__{
      id: generate_connection_id(),
      connected_channels: connected_channels
    }
  end

  @spec terminate(t()) :: any()
  def terminate(%__MODULE__{connected_channels: channels}) do
    Enum.each(channels, fn channel -> Api.create_message(channel, embeds: [Embeds.disconnected()]) end)
  end

  @spec transmit_message(t(), Message.t()) :: any()
  def transmit_message(%__MODULE__{connected_channels: channels}, %Message{} = message) do
    embed =
      %Embed{}
      |> Embed.put_author(message.author.username, nil, User.avatar_url(message.author))
      |> Embed.put_description(message.content)
      |> append_image(message)

    channels
    |> Enum.filter(fn channel -> channel != message.channel_id end)
    |> Enum.each(fn channel -> spawn(fn -> Api.create_message(channel, embeds: [embed]) end) end)
  end

  defp append_image(%Embed{} = embed, %Message{} = message) do
    message.attachments
    |> Enum.filter(fn %Attachment{width: width} -> width != nil end)
    |> Enum.map(fn %Attachment{url: url} -> url end)
    |> Enum.take(1)
    |> Enum.reduce(embed, fn image, embed -> Embed.put_image(embed, image) end)
  end

  @spec generate_connection_id(pos_integer()) :: String.t()
  defp generate_connection_id(length \\ 5) do
    length
    |> MnemonicSlugs.generate_slug()
    |> String.replace("-", " ")
  end
end
