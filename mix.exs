defmodule Wormhole.MixProject do
  use Mix.Project

  def project do
    [
      app: :wormhole,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Wormhole.Supervisor, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nostrum, "~> 0.6.1"},
      {:nosedrum, "~> 0.4"},
      {:mnemonic_slugs, "~> 0.0.3"}
    ]
  end
end
